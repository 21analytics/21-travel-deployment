#!/bin/bash -e

declare -A pw_for_user=(
    [autod]="$AUTOD_DB_PW"
    [aopd]="$AOPD_DB_PW"
    [trpd]="$TRPD_DB_PW"
)

export PGDATABASE=21travel

createdb

for dbuser in autod aopd trpd; do
    psql --quiet --set=ON_ERROR_STOP=1 --command "\
        CREATE USER $dbuser PASSWORD '${pw_for_user[$dbuser]}'; \
        CREATE SCHEMA AUTHORIZATION $dbuser;"
done

for dbuser in aopd trpd; do
    psql --quiet --set=ON_ERROR_STOP=1 --command "\
        GRANT USAGE ON SCHEMA $dbuser TO autod; \
        ALTER DEFAULT PRIVILEGES FOR USER $dbuser IN SCHEMA $dbuser GRANT ALL ON TABLES TO autod;"
done

psql --quiet --set=ON_ERROR_STOP=1 --command "CREATE SCHEMA offline AUTHORIZATION autod;"
psql --quiet --set=ON_ERROR_STOP=1 --command "CREATE SCHEMA email AUTHORIZATION autod;"
psql --quiet --set=ON_ERROR_STOP=1 <<EOF
CREATE TABLE autod.audit_logs (
    id TEXT PRIMARY KEY NOT NULL DEFAULT (gen_random_uuid()),
    username TEXT NOT NULL,
    keycloak_id TEXT NOT NULL,
    event TEXT NOT NULL,
    target TEXT NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT (now())
);
REVOKE ALL ON TABLE autod.audit_logs FROM autod;
GRANT INSERT, SELECT ON TABLE autod.audit_logs TO autod;
EOF


export PGDATABASE=keycloak

createdb

psql --quiet --set=ON_ERROR_STOP=1 --command "\
   CREATE USER keycloak PASSWORD '$KEYCLOAK_DB_PW'; \
   CREATE SCHEMA AUTHORIZATION keycloak;";
