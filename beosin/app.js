// Copyright (c) 2025 21 Analytics AG. All Rights Reserved.

import { createClient } from 'graphqurl'
import { execSync } from 'child_process'

const GRAPHQL_SUBSCRIPTION_URL = new URL('ws://travel-aopd:8766/aopd/graphql-ws')
// 21 Travel Rule secrets
const CLIENT_SECRET = process.env.CLIENT_SECRET
const CLIENT_ID = process.env.CLIENT_ID
// Beosin secrets
const APPID = process.env.APP_ID
const APP_SECRET = process.env.APP_SECRET

if (!CLIENT_SECRET || !CLIENT_ID || !APPID || !APP_SECRET) {
    console.warn(new Date(), 'disabling service: missing required secrets')
    while (true) execSync('sleep 43200')
}

const query = `mutation setProviderRiskScore($id: ID!, $risk: AopdRiskInput!) {
                 aopd {
                   setProviderRiskScore(id: $id, provider: "Beosin", risk: $risk) {
                     __typename
                     ... on AopdSignatureProof {
                       id
                     }
                     ... on AopdMediaProof {
                       id
                     }
                     ... on AopdSatoshiProof {
                       id
                     }
                   }
                 }
               }`

const body = new URLSearchParams()
body.append('grant_type', 'client_credentials')
body.append('scope', 'openid')
body.append('client_id', CLIENT_ID)
body.append('client_secret', CLIENT_SECRET)

const getToken = async () => {
    const data = await fetch(
      'http://keycloak:8080/auth/realms/21travel/protocol/openid-connect/token',
      {
        method: 'POST',
        body,
      },
    )
    const json = await data.json()
    return json.access_token
}

let queryClient
getToken().then((bearer_token) => {
  queryClient = createClient({
    endpoint: 'http://travel-autod:8765/graphql',
    headers: { Authorization: `Bearer ${bearer_token}` },
  })
})

getToken()
  .then((bearer_token) => {
    console.log(new Date(), 'Started Beosin Risk Score service.')
    const client = createClient({
      websocket: {
        endpoint: GRAPHQL_SUBSCRIPTION_URL,
        shouldRetry: true,
        onConnectionError: console.error,
        onConnectionSuccess: () => {
          client.subscribe(
            {
              subscription:
                'subscription AopdAoppProofs { aopdAoppProofs { id \n address \n asset } }',
            },
            async ({ data }) => {
              const riskApiUrl = 'https://api.beosin.com/api/v3/kyt/address/risk'
              const address = data.aopdAoppProofs.address
              const asset = data.aopdAoppProofs.asset
              const id = data.aopdAoppProofs.id

              queryClient.updateHeaders({ Authorization: `Bearer ${await getToken()}` })

              let chainId = undefined
              let kytUrl = 'https://kyt.beosin.com'
              if (asset === 'DTI4H95J0R2X') {
                chainId = '0'
                kytUrl = `https://kyt.beosin.com/address/${address}?platform=BTC`
              } else {
                try {
                  resp = await queryClient.query({
                    query,
                    variables: {
                      id,
                      risk: {
                          score: 'Unsupported Asset',
                          icon: 'UNKNOWN',
                          kytUrl,
                      }
                    },
                  })
                } catch (error) {
                  console.error(error)
                  return
                }
                if (resp.error) console.error(resp.error)
                return
              }

              let resp
              try {
                resp = await fetch(
                  `${riskApiUrl}?chainId=${chainId}&address=${address}&token=`,
                  {
                    method: 'GET',
                    headers: {
                      APPID,
                      'APP-SECRET': APP_SECRET,
                    },
                  },
                )
              } catch (error) {
                console.error('Error calling risk API:', error)
                return
              }
              const riskData = await resp.json()
              if (riskData.code !== 200) throw new Error(riskData.msg)

              try {
                resp = await queryClient.query({
                  query,
                  variables: {
                    id,
                    risk: {
                        score: riskData.data.riskLevel ?? 'Unknown Risk',
                        icon: 'UNKNOWN',
                        kytUrl,
                    }
                  },
                })
              } catch (error) {
                console.error(error)
                return
              }
              if (resp.error) console.error(resp.error)
            },
            console.error,
          )
        },
        parameters: { bearer_token },
      },
    })
  })
