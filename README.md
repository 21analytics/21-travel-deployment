# 21 Travel Rule: Reference Deployment

21 Travel Rule is a software solution for FATF’s Travel Rule by [21 Analytics](https://www.21analytics.ch/).

Click [here](https://docs.21analytics.ch/7.3.0/deployment/deployment.html)
for the [deployment instructions](https://docs.21analytics.ch/7.3.0/deployment/deployment.html).
