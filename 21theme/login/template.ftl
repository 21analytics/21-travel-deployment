<#macro registrationLayout bodyClass="" displayInfo=false displayMessage=true displayRequiredFields=false>
<!DOCTYPE html>
<html class="${properties.kcHtmlClass!}"<#if realm.internationalizationEnabled> lang="${locale.currentLanguageTag}"</#if>>

<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="robots" content="noindex, nofollow">

    <#if properties.meta?has_content>
        <#list properties.meta?split(' ') as meta>
            <meta name="${meta?split('==')[0]}" content="${meta?split('==')[1]}"/>
        </#list>
    </#if>
    <title>${msg("loginTitle",(realm.displayName!''))}</title>
    <link rel="icon" href="${url.resourcesPath}/img/favicon.ico" />
    <#if properties.stylesCommon?has_content>
        <#list properties.stylesCommon?split(' ') as style>
            <link href="${url.resourcesCommonPath}/${style}" rel="stylesheet" />
        </#list>
    </#if>
    <#if properties.styles?has_content>
        <#list properties.styles?split(' ') as style>
            <link href="${url.resourcesPath}/${style}" rel="stylesheet" />
        </#list>
    </#if>
    <#if properties.scripts?has_content>
        <#list properties.scripts?split(' ') as script>
            <script src="${url.resourcesPath}/${script}" type="text/javascript"></script>
        </#list>
    </#if>
    <#if scripts??>
        <#list scripts as script>
            <script src="${script}" type="text/javascript"></script>
        </#list>
    </#if>
</head>

<body class="${properties.kcBodyClass!}">
<div class="${properties.kcLoginClass!}">
    <div id="kc-header" class="${properties.kcHeaderClass!}">
        <div id="kc-header-wrapper" class="${properties.kcHeaderWrapperClass!}">
            <a href="${properties.kcLogoLink!}" target='_blank'>
            <svg
                xmlns='http://www.w3.org/2000/svg'
                width='200'
                height='38'
                viewBox='0 0 535 102'
            >
                <g fill='none' fillRule='evenodd'>
                <g
                    aria-label='ANALYTICS'
                    fontWeight='700'
                    fontSize='65'
                    fontFamily='Lato'
                    fill='#fff'
                >
                    <path
                        d='M194.31 77.493h-7.626q-1.283 0-2.126-.623-.807-.66-1.174-1.614l-3.96-10.816h-21.963l-3.96 10.816q-.293.844-1.173 1.54-.844.697-2.09.697h-7.664l20.827-53.02h10.083zm-34.319-20.02h16.903l-6.453-17.637q-.44-1.173-.99-2.75-.513-1.613-1.027-3.483-.513 1.87-1.026 3.483-.477 1.614-.954 2.824zM205.013 24.473q.66 0 1.1.073.44.037.77.22.367.147.697.477.33.293.733.807l27.83 35.456q-.147-1.283-.22-2.493-.037-1.247-.037-2.31v-32.23h8.69v53.02h-5.096q-1.174 0-1.944-.367-.77-.366-1.503-1.32l-27.72-35.31q.11 1.174.147 2.347.073 1.137.073 2.09v32.56h-8.69v-53.02h5.17zM301.876 77.493h-7.627q-1.283 0-2.126-.623-.807-.66-1.174-1.614l-3.96-10.816h-21.963l-3.96 10.816q-.293.844-1.173 1.54-.844.697-2.09.697h-7.664l20.827-53.02h10.083zm-34.32-20.02h16.903l-6.453-17.637q-.44-1.173-.99-2.75-.513-1.613-1.027-3.483-.513 1.87-1.026 3.483-.477 1.614-.954 2.824zM317.271 69.353h21.23v8.14h-31.093v-53.02h9.863zM360.633 56.886v20.607h-9.863V56.886l-19.323-32.413h8.69q1.283 0 2.016.623.77.624 1.283 1.577l9.717 17.71q.843 1.577 1.467 3.007.623 1.393 1.136 2.75.477-1.394 1.064-2.787.623-1.43 1.466-2.97l9.644-17.71q.403-.807 1.21-1.503.806-.697 2.053-.697h8.727zM422.402 24.473v8.103h-15.95v44.917h-9.864V32.576h-16.023v-8.103zM439.833 77.493h-9.9v-53.02h9.9zM488.23 64.99q.806 0 1.393.623l3.887 4.217q-3.227 3.996-7.957 6.123-4.693 2.127-11.293 2.127-5.904 0-10.634-2.017-4.693-2.017-8.03-5.61-3.336-3.593-5.133-8.58-1.76-4.987-1.76-10.89 0-5.977 1.98-10.927 1.98-4.986 5.573-8.58 3.594-3.593 8.58-5.573 5.024-2.017 11.074-2.017 5.903 0 10.303 1.907 4.437 1.87 7.59 4.987l-3.3 4.583q-.293.44-.77.77-.44.33-1.247.33-.843 0-1.723-.66-.88-.66-2.237-1.43-1.356-.77-3.446-1.43-2.054-.66-5.244-.66-3.74 0-6.893 1.32-3.117 1.283-5.39 3.703-2.237 2.42-3.52 5.904-1.247 3.446-1.247 7.773 0 4.473 1.247 7.957 1.283 3.483 3.447 5.866 2.163 2.384 5.096 3.667 2.934 1.247 6.307 1.247 2.017 0 3.63-.22 1.65-.22 3.007-.697 1.393-.477 2.603-1.21 1.247-.77 2.457-1.87.366-.33.77-.513.403-.22.88-.22zM529.327 34.116q-.403.807-.953 1.137-.513.33-1.247.33-.733 0-1.65-.55-.916-.587-2.163-1.283-1.247-.697-2.933-1.247-1.65-.587-3.924-.587-2.053 0-3.593.514-1.503.476-2.567 1.356-1.026.88-1.54 2.127-.513 1.21-.513 2.677 0 1.87 1.027 3.116 1.063 1.247 2.786 2.127 1.724.88 3.924 1.577 2.2.696 4.473 1.503 2.31.77 4.51 1.833 2.2 1.027 3.923 2.64 1.724 1.577 2.75 3.887 1.064 2.31 1.064 5.61 0 3.593-1.247 6.747-1.21 3.116-3.593 5.463-2.347 2.31-5.757 3.667-3.41 1.32-7.81 1.32-2.53 0-4.987-.514-2.456-.476-4.73-1.393-2.236-.917-4.216-2.2-1.944-1.283-3.484-2.86l2.86-4.73q.404-.513.954-.843.586-.367 1.283-.367.917 0 1.98.77 1.063.733 2.493 1.65 1.467.917 3.41 1.687 1.98.733 4.73.733 4.217 0 6.527-1.98 2.31-2.017 2.31-5.757 0-2.09-1.063-3.41-1.027-1.32-2.75-2.2-1.724-.916-3.924-1.54-2.2-.623-4.473-1.356-2.273-.734-4.473-1.76-2.2-1.027-3.924-2.677-1.723-1.65-2.786-4.107-1.027-2.493-1.027-6.123 0-2.897 1.137-5.647 1.173-2.75 3.373-4.876 2.237-2.127 5.463-3.41 3.227-1.284 7.37-1.284 4.694 0 8.654 1.467t6.746 4.107z'
                        />
                    </g>
                    <path
                        fill='#3E4B52'
                        fillRule='nonzero'
                        d='M16.217 2h91.866c7.852 0 14.218 6.338 14.218 14.156v69.688c0 7.818-6.366 14.156-14.218 14.156H16.217C8.365 100 2 93.662 2 85.844V16.156C2 8.338 8.365 2 16.217 2z'
                    />
                    <path
                        fill='#07F49E'
                        d='M68.822 36.358l9.713-9.158h9.193v47.6h-9.456V38.495l-11.86 11.952c-.27.327-.553.643-.85.95a33.515 33.515 0 01-2.654 2.446l-15.423 12.23H70.01V74.8H34.572V64.288L54.3 47.958a35.687 35.687 0 003.012-2.842c1.005-1.058 1.507-2.292 1.507-3.702 0-1.587-.622-2.832-1.865-3.735-1.244-.904-2.702-1.356-4.376-1.356-2.009 0-3.575.573-4.699 1.72-1.124 1.145-1.757 2.556-1.9 4.23l-10.761-.727c.143-2.424.693-4.528 1.65-6.314a13.496 13.496 0 013.802-4.462c1.578-1.19 3.431-2.082 5.56-2.678 2.127-.595 4.435-.892 6.922-.892 2.295 0 4.447.297 6.456.892 2.009.596 3.754 1.477 5.237 2.645 1.482 1.168 2.642 2.633 3.479 4.396.187.394.353.802.498 1.225z'
                    />
                </g>
            </svg>
        </a>
    </div>
        
    </div>
    <div class="${properties.kcFormCardClass!}">
        <header class="${properties.kcFormHeaderClass!}">
        <#if !(auth?has_content && auth.showUsername() && !auth.showResetCredentials())>
            <#if displayRequiredFields>
                <div class="${properties.kcContentWrapperClass!}">
                    <div class="${properties.kcLabelWrapperClass!} subtitle">
                        <span class="subtitle"><span class="required">*</span> ${msg("requiredFields")}</span>
                    </div>
                    <div class="col-md-10">
                        <h1 id="kc-page-title"><#nested "header"></h1>
                    </div>
                </div>
            <#else>
                <h1 id="kc-page-title"><#nested "header"></h1>
            </#if>
        <#else>
            <#if displayRequiredFields>
                <div class="${properties.kcContentWrapperClass!}">
                    <div class="${properties.kcLabelWrapperClass!} subtitle">
                        <span class="subtitle"><span class="required">*</span> ${msg("requiredFields")}</span>
                    </div>
                    <div class="col-md-10">
                        <#nested "show-username">
                        <div id="kc-username" class="${properties.kcFormGroupClass!}">
                            <label id="kc-attempted-username">${auth.attemptedUsername}</label>
                            <a id="reset-login" href="${url.loginRestartFlowUrl}" aria-label="${msg("restartLoginTooltip")}">
                                <div class="kc-login-tooltip">
                                    <i class="${properties.kcResetFlowIcon!}"></i>
                                    <span class="kc-tooltip-text">${msg("restartLoginTooltip")}</span>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            <#else>
                <#nested "show-username">
                <div id="kc-username" class="${properties.kcFormGroupClass!}">
                    <label id="kc-attempted-username">${auth.attemptedUsername}</label>
                    <a id="reset-login" href="${url.loginRestartFlowUrl}" aria-label="${msg("restartLoginTooltip")}">
                        <div class="kc-login-tooltip">
                            <i class="${properties.kcResetFlowIcon!}"></i>
                            <span class="kc-tooltip-text">${msg("restartLoginTooltip")}</span>
                        </div>
                    </a>
                </div>
            </#if>
        </#if>
      </header>
      <div id="kc-content">
        <div id="kc-content-wrapper">

          <#-- App-initiated actions should not see warning messages about the need to complete the action -->
          <#-- during login.                                                                               -->
          <#if displayMessage && message?has_content && (message.type != 'warning' || !isAppInitiatedAction??)>
              <div class="alert-${message.type} ${properties.kcAlertClass!} pf-m-<#if message.type = 'error'>danger<#else>${message.type}</#if>">
                  <div class="pf-c-alert__icon">
                      <#if message.type = 'success'><span class="${properties.kcFeedbackSuccessIcon!}"></span></#if>
                      <#if message.type = 'warning'><span class="${properties.kcFeedbackWarningIcon!}"></span></#if>
                      <#if message.type = 'error'><span class="${properties.kcFeedbackErrorIcon!}"></span></#if>
                      <#if message.type = 'info'><span class="${properties.kcFeedbackInfoIcon!}"></span></#if>
                  </div>
                      <span class="${properties.kcAlertTitleClass!}">${kcSanitize(message.summary)?no_esc}</span>
              </div>
          </#if>

          <#nested "form">

          <#if auth?has_content && auth.showTryAnotherWayLink()>
              <form id="kc-select-try-another-way-form" action="${url.loginAction}" method="post">
                  <div class="${properties.kcFormGroupClass!}">
                      <input type="hidden" name="tryAnotherWay" value="on"/>
                      <a href="#" id="try-another-way"
                         onclick="document.forms['kc-select-try-another-way-form'].submit();return false;">${msg("doTryAnotherWay")}</a>
                  </div>
              </form>
          </#if>

          <#if displayInfo>
              <div id="kc-info" class="${properties.kcSignUpClass!}">
                  <div id="kc-info-wrapper" class="${properties.kcInfoAreaWrapperClass!}">
                      <#nested "info">
                  </div>
              </div>
          </#if>
        </div>
      </div>

    </div>
  </div>
</body>
</html>
</#macro>
